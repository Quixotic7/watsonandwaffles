﻿using System;
using UnityEngine;
using System.Collections;
using IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3;
using UnityEngine.Events;

public class WatsonClassifyDrawing : MonoBehaviour
{
    [Serializable]
    public class DrawingData
    {
        public float heartScore;
        public float lightbulbScore;
        public float keyScore;
        public float laddedScore;
    }

	public enum DrawingClass
	{
		Heart,
		Lightbulb,
		Key,
		Ladder
	}

	public static UnityAction<DrawingClass> onClassified;
    public static UnityAction onHeartClassified;
    public static UnityAction onLightbulbClassified;
    public static UnityAction onKeyClassified;
    public static UnityAction onLadderClassified;

    public UnityEvent onClassifyEvent;

    public AudioSource shutterSound;

    public ViveControllerDrawingTool3D[] drawingTools;

    public bool doClassify = false;

    public bool enabledDebugControls = true;

    protected string classifierID = "ingamedrawings_1524330060";

    protected bool classificationInProgress;

    protected VisualRecognition visualRecognition = new VisualRecognition();


    protected void OnClassify(DrawingClass drawingClass)
    {
        
    }

    protected void OnHeartClassified()
    {
        Debug.Log("Heart detected");
    }

    protected void OnLightbulbClassified()
    {
        Debug.Log("Lightbulb detected");
    }

    protected void OnKeyClassified()
    {
        Debug.Log("Key detected");
    }

    protected void OnLadderClassified()
    {
        Debug.Log("Ladder detected");
    }

    protected void Start()
    {
        onClassified += OnClassify;
        onHeartClassified += OnHeartClassified;
        onLightbulbClassified += OnLightbulbClassified;
        onKeyClassified += OnKeyClassified;
        onLadderClassified += OnLadderClassified;
    }

    protected void Update()
    {
        if (enabledDebugControls)
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                if (onLightbulbClassified != null)
                {
                    onLightbulbClassified.Invoke();
                }
            }
            if (Input.GetKeyDown(KeyCode.H))
            {
                onHeartClassified.Invoke();
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                onLadderClassified.Invoke();
            }
            if (Input.GetKeyDown(KeyCode.K))
            {
                onKeyClassified.Invoke();
            }
        }
    }

    public void ClassifyDrawing(byte[] imageAsJPG)
    {
        if (classificationInProgress) return;

        shutterSound.Play();

        StartCoroutine(DetectClassifiersOnImage(imageAsJPG));
    }

    protected IEnumerator DetectClassifiersOnImage(byte[] imageData)
    {
        yield return new WaitForEndOfFrame();

        Debug.Log("Detecting classifiers");

        //if (classifySpecificImageIndex < 0 || classifySpecificImageIndex >= people.Length)
        //{
        //    Debug.Log("Index out of range!");
        //    classificationInProgress = false;
        //    yield return null;
        //}

        classificationInProgress = true;

        //currentPersonIndex = classifySpecificImageIndex;

        if (doClassify)
        {
            string[] owners = { "me" };
            string[] classifierIDs = { classifierID };

        //people[classifySpecificImageIndex].emotionData = new FaceEmotionData();

        
            if (!visualRecognition.Classify(OnClassify, imageData, owners, classifierIDs))
            {
                Debug.Log("Classify image failed!");
                classificationInProgress = false;
            }

            while (classificationInProgress)
            {
                yield return new WaitForEndOfFrame();
            }
        }

        classificationInProgress = false;

        if(onClassifyEvent != null)
            onClassifyEvent.Invoke();

        foreach (var drawingTool in drawingTools)
        {
            drawingTool.OnClassifyDone();
        }
    }

    private void OnClassify(ClassifyTopLevelMultiple classify, string data)
    {
        Debug.Log("OnClassify");

        //if (currentPersonIndex < 0 || currentPersonIndex >= people.Length)
        //{
        //    Debug.Log("Index out of range!");
        //    detectingFace = false;
        //    return;
        //}

        //var dataContainer = people[currentPersonIndex].emotionData;

        if (classify != null)
        {
            Debug.Log("Classify success!");

            Debug.Log("ExampleVisualRecognition images processed: " + classify.images_processed);
            foreach (ClassifyTopLevelSingle image in classify.images)
            {
                // Debug.Log("ExampleVisualRecognition source_url: " + image.source_url + ", resolved_url: " + image.resolved_url);
                Debug.Log("ExampleVisualRecognition image classifiers: " + image.classifiers.Length);

                if (image.classifiers != null && image.classifiers.Length > 0)
                {
                    foreach (ClassifyPerClassifier classifier in image.classifiers)
                    {
                        Debug.Log("classifier_id: " + classifier.classifier_id + ", name: " + classifier.name);
                        foreach (ClassResult classResult in classifier.classes)
                        {
                            Debug.Log("ExampleVisualRecognition class: " + classResult.m_class + ", score: " +
                                      classResult.score + ", type_hierarchy: " + classResult.type_hierarchy);

                            float posThreshold = 0.15f;

                            if (classResult.m_class == "lightbulb" && classResult.score >= posThreshold)
                            {
                                onLightbulbClassified.Invoke();
                                // onClassified.Invoke(DrawingClass.Lightbulb);
                            }

                            if (classResult.m_class == "heart" && classResult.score >= posThreshold)
                            {
                                onHeartClassified.Invoke();
                                onClassified.Invoke(DrawingClass.Heart);
                            }

                            if (classResult.m_class == "key" && classResult.score >= posThreshold)
                            {
                                onKeyClassified.Invoke();
                                onClassified.Invoke(DrawingClass.Key);
                            }

                            if (classResult.m_class == "ladder" && classResult.score >= posThreshold)
                            {
                                onLadderClassified.Invoke();
	                            //onClassified(DrawingClass.Ladder);

                                onClassified.Invoke(DrawingClass.Ladder);
                            }

                            //if (classResult.m_class == "happy")
                            //{
                            //    dataContainer.happyScore = (float)classResult.score;
                            //}
                            //if (classResult.m_class == "angry")
                            //{
                            //    dataContainer.angryScore = (float)classResult.score;
                            //}
                            //if (classResult.m_class == "eyewear")
                            //{
                            //    dataContainer.glassesScore = (float)classResult.score;
                            //}
                        }
                    }
                }
            }
        }
        else
        {
            Debug.Log("Classification failed!");
        }

        classificationInProgress = false;
    }
}
