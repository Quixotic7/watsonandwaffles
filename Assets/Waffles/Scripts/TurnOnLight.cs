﻿using UnityEngine;
using System.Collections;

public class TurnOnLight : MonoBehaviour
{
    public bool lightsOn;

    protected Light light;

    protected void Start()
    {
        light = GetComponent<Light>();
        if (light == null)
        {
            Debug.LogError("Need a light component");
            return;
        }

        light.enabled = lightsOn;

        WatsonClassifyDrawing.onLightbulbClassified += OnLightBulbClassified;
    }

    protected void OnLightBulbClassified()
    {
        lightsOn = !lightsOn;

        Debug.Log("Lights: " + lightsOn);

        light.enabled = lightsOn;
    }
}
