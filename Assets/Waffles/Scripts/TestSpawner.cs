﻿using UnityEngine;

[RequireComponent(typeof(ObjectSpawner))]
public class TestSpawner : MonoBehaviour {
	private static readonly KeyCode spawnHeart = KeyCode.Alpha1;
	private static readonly KeyCode spawnKey = KeyCode.Alpha2;
	private static readonly KeyCode spawnLadder = KeyCode.Alpha3;
	private ObjectSpawner spawner;

	void Awake()
	{
		spawner = GetComponent<ObjectSpawner>();
	}

	void Update()
	{
		if (Input.GetKeyDown(spawnHeart))
		{
			spawner.OnClassify(WatsonClassifyDrawing.DrawingClass.Heart);
		}

		if (Input.GetKeyDown(spawnKey))
		{
			spawner.OnClassify(WatsonClassifyDrawing.DrawingClass.Key);
		}

		if (Input.GetKeyDown(spawnLadder))
		{
			spawner.OnClassify(WatsonClassifyDrawing.DrawingClass.Ladder);
		}
	}
}
