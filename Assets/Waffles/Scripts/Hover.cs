﻿using UnityEngine;

public class Hover : MonoBehaviour {
	[SerializeField] [Range(0.0f, 1.0f)] public float amplitude;
	[SerializeField] [Range(0.01f, 1.0f)] public float speed;
	private Vector3 initialPosition;
	void Awake()
	{
		initialPosition = transform.position;
	}
	void Update () {
		var verticalOffset = amplitude * Mathf.Sin(speed * Time.time);
		transform.position = initialPosition + (Vector3.up * verticalOffset);
	}
}
