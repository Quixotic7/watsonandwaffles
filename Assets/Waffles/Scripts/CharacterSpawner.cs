﻿using UnityEngine;
using System.Collections;

public class CharacterSpawner : MonoBehaviour
{
    public ClassifyFaces classifyFaces;
    public ClassifyFaces.Person personData;


    public int femaleAge1Clamp = 17;
    public GameObject[] femaleAge1;

    public int femaleAge2Clamp = 25;
    public GameObject[] femaleAge2;

    public int femaleAge3Clamp = 80;
    public GameObject[] femaleAge3;


    public int maleAge1Clamp = 17;
    public GameObject[] maleAge1;

    public int maleAge2Clamp = 30;
    public GameObject[] maleAge2;

    public int maleAge3Clamp = 80;
    public GameObject[] maleAge3;

    public GameObject[] maleFabulous;

    public static int personIndex = 0;


    public Transform faceMask;

    protected Vector3 faceMaskLocalPosition = new Vector3(0.0004f, 0.0636f, 0.1121f);

    public MeshRenderer faceMaskRenderer;

    // Use this for initialization
    void Start () {

        if (classifyFaces == null)
        {
            Debug.LogError("ClassifyFaces is null");
            return;
        }
        if (classifyFaces.people == null || classifyFaces.people.Length == 0)
        {
            Debug.LogError("Classify faces has no people classified. Go make some people and come back to me");
            return;
        }

        StartCoroutine(WaitThenSpawn());
    }

    public IEnumerator WaitThenSpawn()
    {
        yield return new WaitForSeconds(Random.value);

        if (personIndex >= classifyFaces.people.Length)
        {
            Debug.LogError("Cannot Spawn person, not enough people to choose from");
            yield return null;
        }

        personData = classifyFaces.people[personIndex];

        personIndex++;

        if (personData.faceDetectionData.gender == " ") yield return null;

        if (personData.faceDetectionData.gender == "MALE")
        {
            if (personData.faceDetectionData.identity != "")
            {
                SpawnCharacter(maleFabulous);
            }
            else
            {
                if (personData.faceDetectionData.ageMax <= maleAge1Clamp)
                {
                    SpawnCharacter(maleAge1);
                }
                else if (personData.faceDetectionData.ageMax <= maleAge2Clamp)
                {
                    SpawnCharacter(maleAge2);
                }
                else if (personData.faceDetectionData.ageMax <= maleAge3Clamp)
                {
                    SpawnCharacter(maleAge3);
                }
                else
                {
                    SpawnCharacter(maleAge3);
                }
            }
        }
        else
        {
            if (personData.faceDetectionData.ageMax <= femaleAge1Clamp)
            {
                SpawnCharacter(femaleAge1);
            }
            else if (personData.faceDetectionData.ageMax <= femaleAge2Clamp)
            {
                SpawnCharacter(femaleAge2);
            }
            else if (personData.faceDetectionData.ageMax <= femaleAge3Clamp)
            {
                SpawnCharacter(femaleAge3);
            }
            else
            {
                SpawnCharacter(femaleAge3);
            }
        }

    }

    public void SpawnCharacter(GameObject[] gameObjects)
    {
        if (gameObjects == null || gameObjects.Length == 0)
        {
            Debug.LogError("Failed to spawn, no GameObjects in array");
            return;
        }

        var randomIndex = Random.Range(0, gameObjects.Length);

        if (randomIndex == gameObjects.Length) randomIndex = Mathf.Max(gameObjects.Length - 1, 0);

        var newGo = Instantiate(gameObjects[randomIndex], transform, true) as GameObject;

        if (newGo == null) return;

        newGo.transform.position = transform.position;
        newGo.transform.rotation = transform.rotation;

        var faceLinker = newGo.GetComponent<FaceLinker>();

        if (faceLinker == null)
        {
            Debug.LogError("Spawned character has no face linker attached");
            return;
        }

        faceMask.transform.position = faceLinker.headTransform.position;
        faceMask.transform.rotation = faceLinker.headTransform.rotation;
        faceMask.transform.parent = faceLinker.headTransform;
        faceMask.transform.localPosition = faceMaskLocalPosition;

        var faceManager = newGo.AddComponent<CharacterFaceManager>();

        faceManager.DoIt(personData, faceMaskRenderer);
        
        //var faceManager = newGo.GetComponent<CharacterFaceManager>();

        //if (faceManager == null)
        //{
        //    Debug.LogError("Spawned character has no face manager attached");
        //    return;
        //}

        // faceManager.DoIt(personData);
    }
}
