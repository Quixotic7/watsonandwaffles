﻿using UnityEngine;
using System.Collections;

public class CharacterFaceManager : MonoBehaviour
{
    //public ClassifyFaces classifyFaces;

    protected MeshRenderer faceMaskRenderer;

    public ClassifyFaces.Person personData;

    protected Material materialInstance;

    protected Vector2 ageRange = new Vector2(0.8f, 1.4f);

    public float averageAge;

    public void Start()
    {
        // personData = classifyFaces.people[0];
    }

    public void DoIt(ClassifyFaces.Person newPersonData, MeshRenderer maskRenderer)
    {
        personData = newPersonData;


        faceMaskRenderer = maskRenderer;

        materialInstance = new Material(faceMaskRenderer.material);

        faceMaskRenderer.material = materialInstance;

        materialInstance.mainTexture = personData.texture2D;

        CropToFace();

        averageAge = personData.faceDetectionData.ageMin + personData.faceDetectionData.ageMax;

        UpdatePlayerScale();
    }

    protected void Update()
    {
        
    }

    protected void UpdatePlayerScale()
    {
        var t = averageAge / 100;

        var scale = Vector3.Lerp(Vector3.one * ageRange.x, Vector3.one * ageRange.y, t);

        transform.localScale = scale;
    }

    public void CropToFace()
    {
        var f = personData.faceDetectionData;

        var tex = personData.texture2D;

        var imageWidth = tex.width;
        var imageHeight = tex.height;

        var newLeft = Mathf.Max((f.cropLeft + f.cropWidth * 0.5f) - (f.cropHeight * 0.5f), 0);

        var w = (float)Mathf.Max(f.cropHeight, 1) / tex.width;
        var h = (float)Mathf.Max(f.cropHeight, 1) / tex.height;

        var offX = (float)Mathf.Max(newLeft, 1) / tex.width;
        var offY = (float)Mathf.Max(f.cropTop + f.cropHeight, 1) / tex.height;

        materialInstance.mainTextureOffset = new Vector2(offX, -offY);
        materialInstance.mainTextureScale = new Vector2(w, h);
    }
}
