﻿using System;
using UnityEngine;
using System.Collections;
using IBM.Watson.DeveloperCloud.Logging;
using IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1;
using IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3;
using IBM.Watson.DeveloperCloud.Utilities;

public class CropFace : MonoBehaviour
{
    [SerializeField]
    private Texture2D faceTexture;

    VisualRecognition m_VisualRecognition;

    protected Material materialInstance;

    public FaceData faceResult;

    protected string emotionsClassifierID = "facial_emotions_945249405";


    public int cropLeft;
    public int cropTop;
    public int cropWidth;
    public int cropHeight;

    public int imageWidth;
    public int imageHeight;

    protected bool detectingFaces;
    protected bool detectingEmotions;


    // Use this for initialization
    void Start ()
    {
        var renderer = GetComponent<MeshRenderer>();

        materialInstance = new Material(renderer.material);

        renderer.material = materialInstance;

        m_VisualRecognition = new VisualRecognition();
    }

    // Update is called once per frame
    void Update ()
	{
	    if (Input.GetKeyDown(KeyCode.W))
	    {
            AskWatsonToRecognizeFace();
	    }


        if (Input.GetKeyDown(KeyCode.C))
        {
            CropToFace(faceResult);
        }

        // CropImage();
    }

    public void AskWatsonToRecognizeFace()
    {
        Log.Debug("CropFace", "Attempting to detect faces");
        Runnable.Run(DetectFacesInImage());
    }

    public void CropImage()
    {
        var tex = materialInstance.mainTexture;

        imageWidth = tex.width;
        imageHeight = tex.height;

        var newLeft = Mathf.Max((cropLeft + cropWidth*0.5f) - (cropHeight*0.5f), 0);

        var w = (float)Mathf.Max(cropHeight, 1) / tex.width;
        var h = (float)Mathf.Max(cropHeight, 1) / tex.height;

        var offX = (float) Mathf.Max(newLeft, 1) / tex.width;
        var offY = (float) Mathf.Max(cropTop, 1) / tex.height;

        materialInstance.mainTextureOffset = new Vector2(offX, offY);
        materialInstance.mainTextureScale = new Vector2(w, h);
    }


    public void CropToFace(FaceData face)
    {
        var f = face;

        var tex = materialInstance.mainTexture;

        imageWidth = tex.width;
        imageHeight = tex.height;

        var newLeft = Mathf.Max((f.cropLeft + f.cropWidth * 0.5f) - (f.cropHeight * 0.5f), 0);

        var w = (float)Mathf.Max(f.cropHeight, 1) / tex.width;
        var h = (float)Mathf.Max(f.cropHeight, 1) / tex.height;

        var offX = (float)Mathf.Max(newLeft, 1) / tex.width;
        var offY = (float)Mathf.Max(f.cropTop + f.cropHeight, 1) / tex.height;

        materialInstance.mainTextureOffset = new Vector2(offX, -offY);
        materialInstance.mainTextureScale = new Vector2(w, h);
    }


    //public Texture2D CropImage(Texture2D inTexture, float left, float top, int width, int height)
    //{
    //    new Texture2D(width, height, inTexture.format, false);

    //    var pixels = inTexture.GetPixels();




    //}


    public void DetectFaces()
    {
        Log.Debug("WebCamRecognition", "Attempting to detect faces!");
        Runnable.Run(DetectFacesInImage());
    }

    private IEnumerator DetectFacesInImage()
    {
        yield return new WaitForEndOfFrame();

        byte[] imageData = faceTexture.EncodeToPNG();


        string imgPath = Application.streamingAssetsPath + "Faces/testFace_001.jpg";

        //Debug.Log("DataPath: " + imgPath);

        //m_VisualRecognition.DetectFaces(OnDetectFaces, imgPath, null);



        //detectingFaces = true;

        //if (!m_VisualRecognition.DetectFaces(OnDetectFaces, imageData))
        //{
        //    Debug.Log("Detect images failed!");
        //    detectingFaces = false;
        //}

        //while (detectingFaces)
        //{
        //    yield return new WaitForEndOfFrame();
        //}
        Debug.Log("Classifying image!");

        string[] owners = { "me" };
        string[] classifierIDs = { emotionsClassifierID };

        detectingEmotions = true;

        if (!m_VisualRecognition.Classify(OnClassify, imageData, owners, classifierIDs))
        {
            Debug.Log("Classify image failed!");
            detectingEmotions = false;
        }

        //m_VisualRecognition.DetectFaces(OnDetectFaces, ApplicationData.)

        //yield return new WaitForEndOfFrame();

        //Texture2D image = new Texture2D(m_WebCamWidget.WebCamTexture.width, m_WebCamWidget.WebCamTexture.height, TextureFormat.RGB24, false);
        //image.SetPixels32(m_WebCamWidget.WebCamTexture.GetPixels32());

        //byte[] imageData = image.EncodeToPNG();

        //m_VisualRecognition.DetectFaces(OnDetectFaces, imageData);
    }

    private void OnClassify(ClassifyTopLevelMultiple classify, string data)
    {
        if (classify != null)
        {
            Debug.Log("Classify success!");

            Debug.Log("ExampleVisualRecognition images processed: " + classify.images_processed);
            foreach (ClassifyTopLevelSingle image in classify.images)
            {
                Debug.Log("ExampleVisualRecognition source_url: " + image.source_url + ", resolved_url: " + image.resolved_url);
                Debug.Log("ExampleVisualRecognition image classifiers: " + image.classifiers.Length);

                if (image.classifiers != null && image.classifiers.Length > 0)
                {
                    foreach (ClassifyPerClassifier classifier in image.classifiers)
                    {
                        Log.Debug("ExampleVisualRecognition", "\t\tclassifier_id: " + classifier.classifier_id + ", name: " + classifier.name);
                        foreach (ClassResult classResult in classifier.classes)
                            Log.Debug("ExampleVisualRecognition", "\t\t\tclass: " + classResult.m_class + ", score: " + classResult.score + ", type_hierarchy: " + classResult.type_hierarchy);
                    }
                }
            }
        }
        else
        {
            Debug.Log("Classification failed!");
        }

        detectingEmotions = false;
    }



    private void OnDetectFaces(FacesTopLevelMultiple multipleImages, string data)
    {
        Debug.Log("OnDetectFaces");

        if (multipleImages != null)
        {
            Log.Debug("WebCamRecognition", "images processed: {0}", multipleImages.images_processed);

            Debug.Log("Images processed: " + multipleImages.images_processed);
            if (multipleImages.images != null)
            {
                Debug.Log("FaceCount: " + multipleImages.images.Length);

                foreach (FacesTopLevelSingle faces in multipleImages.images)
                {
                    Log.Debug("WebCamRecognition", "\tsource_url: {0}, resolved_url: {1}", faces.source_url,
                        faces.resolved_url);
                    if (faces.faces != null)
                    {
                        foreach (OneFaceResult face in faces.faces)
                        {
                            var result = new FaceData();

                            if (face.face_location != null)
                            {
                                Debug.Log("Face Location: " + face.face_location.left + ", " + ", " + ", ");
                                Log.Debug("WebCamRecognition", "\t\tFace location: {0}, {1}, {2}, {3}",
                                    face.face_location.left,
                                    face.face_location.top, face.face_location.width, face.face_location.height);

                                result.cropLeft = (int) face.face_location.left;
                                result.cropTop = (int) face.face_location.top;
                                result.cropWidth = (int) face.face_location.width;
                                result.cropHeight = (int) face.face_location.height;
                            }
                            if (face.gender != null)
                            {
                                Debug.Log("Gender: " + face.gender.gender);
                                Log.Debug("WebCamRecognition", "\t\tGender: {0}, Score: {1}", face.gender.gender,
                                    face.gender.score);

                                result.gender = face.gender.gender;
                                result.genderScore = (float) face.gender.score;
                            }
                            if (face.age != null)
                            {
                                Log.Debug("WebCamRecognition", "\t\tAge Min: {0}, Age Max: {1}, Score: {2}",
                                    face.age.min,
                                    face.age.max, face.age.score);

                                result.ageMin = face.age.min;
                                result.ageMax = face.age.max;
                                result.ageScore = (float) face.age.score;
                            }

                            if (face.identity != null)
                            {
                                Log.Debug("WebCamRecognition", "\t\tName: {0}, Score: {1}, Type Heiarchy: {2}",
                                    face.identity.name, face.identity.score, face.identity.type_hierarchy);

                                result.identity = face.identity.name;
                                result.identityScore = (float) face.identity.score;
                                result.typeHierarchy = face.identity.type_hierarchy;
                            }

                            faceResult = result;
                        }
                    }
                    else
                    {
                        Debug.Log("Faces.faces is null: " + faces.error.description);

                    }
                }
            }
        }
        else
        {
            Debug.Log("Detect faces failed");

            Log.Debug("WebCamRecognition", "Detect faces failed!");
        }

        detectingFaces = false;
    }
}
