﻿using UnityEngine;

public class Rotate : MonoBehaviour {
	[SerializeField] [Range(0.01f, 2.0f)] public float speed;

	void Update () {
		transform.Rotate(Vector3.up, speed);
	}
}
