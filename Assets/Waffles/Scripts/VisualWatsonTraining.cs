﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3;

public class VisualWatsonTraining : MonoBehaviour
{
    public string classifierName = "WatsonWaffles";
    public string className = "giraffe";

    public string positiveExamplesPath =
        "/Watson/Examples/ServiceExamples/TestData/visual-recognition-classifiers/giraffe_positive_examples.zip";

    public string negativeExamplesPath =
        "/Watson/Examples/ServiceExamples/TestData/visual-recognition-classifiers/negative_examples.zip";

    protected VisualRecognition visualRecognition = new VisualRecognition();

    // Use this for initialization
    void Start () {

        ////          Train classifier
        //Log.Debug("ExampleVisualRecognition", "Attempting to train classifier");
        //string positiveExamplesPath = Application.dataPath + "/Watson/Examples/ServiceExamples/TestData/visual-recognition-classifiers/giraffe_positive_examples.zip";
        //string negativeExamplesPath = Application.dataPath + "/Watson/Examples/ServiceExamples/TestData/visual-recognition-classifiers/negative_examples.zip";


        //Dictionary<string, string> positiveExamples = new Dictionary<string, string>();
        //positiveExamples.Add("giraffe", positiveExamplesPath);
        //if (!m_VisualRecognition.TrainClassifier(OnTrainClassifier, "unity-test-classifier-example", positiveExamples, negativeExamplesPath))
        //    Log.Debug("ExampleVisualRecognition", "Train classifier failed!");

    }

    // Update is called once per frame
    void Update () {
	
	}

    protected void OnGUI()
    {
        var labelWidth = 200;
        var dataWidth = 800;
        GUILayout.BeginArea(new Rect(50, 50, 1400, 1200));

        GUILayout.BeginHorizontal();
        GUILayout.Label("ClassifierNames: ", GUILayout.Width(labelWidth));
        classifierName = GUILayout.TextField(classifierName, GUILayout.Width(dataWidth));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("className: ", GUILayout.Width(labelWidth));
        className = GUILayout.TextField(className, GUILayout.Width(dataWidth));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("positiveExamplesRelativePath: ", GUILayout.Width(labelWidth));
        positiveExamplesPath = GUILayout.TextField(positiveExamplesPath, GUILayout.Width(dataWidth));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("negativeExamplesRelativePath: ", GUILayout.Width(labelWidth));
        negativeExamplesPath = GUILayout.TextField(negativeExamplesPath, GUILayout.Width(dataWidth));
        GUILayout.EndHorizontal();

        if (GUILayout.Button("Train Classifier", GUILayout.Width(400)))
        {
            OnTrainClassifier();   
        }

        GUILayout.EndArea();
    }

    protected void OnTrainClassifier()
    {
        var posPath = Application.dataPath + positiveExamplesPath;
        var negPath = Application.dataPath + negativeExamplesPath;

        var positiveExamples = new Dictionary<string, string>();
        positiveExamples.Add(className, posPath);

        if (negativeExamplesPath == "") negPath = null;

        if (!visualRecognition.TrainClassifier(OnTrainClassifier, classifierName, positiveExamples, negPath, null))
        {
            Debug.Log("Training classifier failed");
        }

    }


    private void OnTrainClassifier(GetClassifiersPerClassifierVerbose classifier, string data)
    {
        if (classifier != null)
        {
           Debug.Log("ExampleVisualRecognition " + classifier + " Classifier is training! ");
        }
        else
        {
            Debug.Log("ExampleVisualRecognition Failed to train classifier! " + data);
        }
    }
}
