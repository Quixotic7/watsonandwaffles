﻿using UnityEngine;
using System.Collections;
using VRTK;

[RequireComponent(typeof(VRTK_ControllerEvents))]
public class LinkControllerToScreenshotter : MonoBehaviour {
    private VRTK_ControllerEvents events;

    public ScreenShotter screenShotter;

    void Awake()
    {
        events = GetComponent<VRTK_ControllerEvents>();
        //events.TouchpadTouchStart += OnTouchpadTouchStart;
        //events.TouchpadTouchEnd += OnTouchpadTouchEnd;

        events.TriggerClicked += OnTakeScreenshot;
    }

    protected void OnTakeScreenshot(object sender, ControllerInteractionEventArgs e)
    {
        screenShotter.TakeScreenShot();
    }
}
