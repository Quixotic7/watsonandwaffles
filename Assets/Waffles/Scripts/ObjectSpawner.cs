﻿using UnityEngine;
using System.Collections.Generic;

public class ObjectSpawner : MonoBehaviour
{
	[SerializeField] private GameObject HeartPrefab;
	[SerializeField] private GameObject KeyPrefab;
	[SerializeField] private GameObject LadderPrefab;
	[SerializeField] private GameObject FlashlightPrefab;

	private Dictionary<WatsonClassifyDrawing.DrawingClass, GameObject> ObjectForDrawingClass;

	void Awake()
	{
		ObjectForDrawingClass = new Dictionary<WatsonClassifyDrawing.DrawingClass, GameObject>
		{
			{WatsonClassifyDrawing.DrawingClass.Heart, HeartPrefab},
			{WatsonClassifyDrawing.DrawingClass.Key, KeyPrefab},
			{WatsonClassifyDrawing.DrawingClass.Ladder, LadderPrefab},
		};
	}

    protected void Start()
    {
        WatsonClassifyDrawing.onClassified += OnClassify;
    }

	public void OnClassify(WatsonClassifyDrawing.DrawingClass drawingClass)
	{
		if (ObjectForDrawingClass.ContainsKey(drawingClass))
		{
			InstantiateInFront(ObjectForDrawingClass[drawingClass]);
		}
	}

	private void InstantiateInFront(GameObject prefab)
	{
		var go = Instantiate(prefab);
		go.transform.position = transform.position;
		// go.transform.localScale = Vector3.one*0.1f;

		//var hover = go.AddComponent<Hover>();
		//hover.amplitude = 0.05f;
		//hover.speed = 3.0f;

		//var rotate = go.AddComponent<Rotate>();
		//rotate.speed = 0.5f;
	}
}
