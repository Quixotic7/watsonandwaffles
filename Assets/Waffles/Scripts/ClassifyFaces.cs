﻿using System;
using UnityEngine;
using System.Collections;
using IBM.Watson.DeveloperCloud.Logging;
using IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1;
using IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3;
using IBM.Watson.DeveloperCloud.Utilities;

[Serializable]
public class FaceData
{
    public int cropLeft;
    public int cropTop;
    public int cropWidth;
    public int cropHeight;

    public string gender;
    public float genderScore;

    public int ageMin;
    public int ageMax;
    public float ageScore;

    public string identity;
    public float identityScore;
    public string typeHierarchy;
}

[Serializable]
public class FaceEmotionData
{
    public float happyScore;
    public float angryScore;

    public float glassesScore;
    public float beardScore;
}

public class ClassifyFaces : MonoBehaviour {

    [Serializable]
    public class Person
    {
        public Texture2D texture2D;

        public FaceData faceDetectionData;

        public FaceEmotionData emotionData;
    }

    protected string classifierID = "facial_emotions_945249405";

    public Person[] people;

    public enum EFaceClassifyMode
    {
        none,
        classifyFaces,
        classifyEmotions
    }
    public EFaceClassifyMode faceClassifyMode;

    public bool classifyOnAwake;
    public bool classifySingleFace;
    public int classifySpecificImageIndex;

    protected VisualRecognition visualRecognition = new VisualRecognition();


    protected void Start()
    {
        detectionInProcess = false;

        if (classifyOnAwake)
        {
            switch (faceClassifyMode)
            {
                case EFaceClassifyMode.none:
                    break;
                case EFaceClassifyMode.classifyFaces:
                    OnClassifyFaces();
                    break;
                case EFaceClassifyMode.classifyEmotions:
                    OnClassifyEmotions();
                    break;
            }
        }
    }

    protected bool detectionInProcess;

    protected void OnClassifyFaces()
    {
        if (detectionInProcess) return;

        detectionInProcess = true;

        Debug.Log("Attempting to detect faces");

        if (classifySingleFace)
        {
            StartCoroutine(DetectSpecificFace());
        }
        else
        {
            StartCoroutine(DetectAllFaces());
        }
    }

    protected void OnClassifyEmotions()
    {
        if (detectionInProcess) return;

        detectionInProcess = true;

        Debug.Log("Attempting to detect emotions");

        if (classifySingleFace)
        {
            StartCoroutine(DetectEmotionsOnSpecificFace());
        }
        else
        {
            StartCoroutine(DetectEmotionsOnAllFaces());
        }
    }

    protected bool detectingFace;

    protected int currentPersonIndex;

    protected IEnumerator DetectSpecificFace()
    {
        yield return new WaitForEndOfFrame();

        if (classifySpecificImageIndex < 0 || classifySpecificImageIndex >= people.Length)
        {
            Debug.Log("Index out of range!");
            yield return null;
        }

        detectingFace = true;

        currentPersonIndex = classifySpecificImageIndex;

        byte[] imageData = people[classifySpecificImageIndex].texture2D.EncodeToJPG();

        people[classifySpecificImageIndex].faceDetectionData = new FaceData();

        if (!visualRecognition.DetectFaces(OnDetectFaces, imageData))
        {
            Debug.Log("Detect images failed!");
            detectingFace = false;
        }

        while (detectingFace)
        {
            yield return new WaitForEndOfFrame();
        }

        detectionInProcess = false;
    }

    protected IEnumerator DetectAllFaces()
    {
        yield return new WaitForEndOfFrame();

        if (people.Length == 0)
        {
            Debug.Log("There are no people to classify");
            yield return null;
        }

        for (var i = 0; i < people.Length; i++)
        {
            detectingFace = true;

            currentPersonIndex = i;

            byte[] imageData = people[i].texture2D.EncodeToJPG();

            people[i].faceDetectionData = new FaceData();

            if (!visualRecognition.DetectFaces(OnDetectFaces, imageData))
            {
                Debug.Log("Detect image at index " + i + " failed!");
                detectingFace = false;
            }

            while (detectingFace)
            {
                yield return new WaitForEndOfFrame();
            }
        }

        Debug.Log("Classifying complete");

        detectionInProcess = false;
    }


    protected IEnumerator DetectEmotionsOnSpecificFace()
    {
        yield return new WaitForEndOfFrame();

        if (classifySpecificImageIndex < 0 || classifySpecificImageIndex >= people.Length)
        {
            Debug.Log("Index out of range!");
            yield return null;
        }

        detectingFace = true;

        currentPersonIndex = classifySpecificImageIndex;

        byte[] imageData = people[classifySpecificImageIndex].texture2D.EncodeToJPG();

        string[] owners = { "me" };
        string[] classifierIDs = { classifierID };

        people[classifySpecificImageIndex].emotionData = new FaceEmotionData();

        if (!visualRecognition.Classify(OnClassify, imageData, owners, classifierIDs))
        {
            Debug.Log("Classify image failed!");
            detectingFace = false;
        }

        while (detectingFace)
        {
            yield return new WaitForEndOfFrame();
        }

        detectionInProcess = false;
    }

    protected IEnumerator DetectEmotionsOnAllFaces()
    {
        yield return new WaitForEndOfFrame();

        if (classifySpecificImageIndex < 0 || classifySpecificImageIndex >= people.Length)
        {
            Debug.Log("Index out of range!");
            yield return null;
        }

        for (var i = 0; i < people.Length; i++)
        {
            detectingFace = true;

            currentPersonIndex = i;

            byte[] imageData = people[i].texture2D.EncodeToJPG();

            string[] owners = { "me" };
            string[] classifierIDs = { classifierID };

            people[i].emotionData = new FaceEmotionData();
        
            if (!visualRecognition.Classify(OnClassify, imageData, owners, classifierIDs))
            {
                Debug.Log("Classify image at index " + i + " failed!");

                detectingFace = false;
            }

            while (detectingFace)
            {
                yield return new WaitForEndOfFrame();
            }
        }

        Debug.Log("Classifying complete");

        detectionInProcess = false;
    }

    private void OnDetectFaces(FacesTopLevelMultiple multipleImages, string data)
    {
        Debug.Log("OnDetectFaces");

        if (currentPersonIndex < 0 || currentPersonIndex >= people.Length)
        {
            Debug.Log("Index out of range!");
            detectingFace = false;
            return;
        }

        var dataContainer = people[currentPersonIndex].faceDetectionData;

        if (multipleImages != null)
        {
            Debug.Log("Images processed: " + multipleImages.images_processed);
            if (multipleImages.images != null)
            {
                Debug.Log("FaceCount: " + multipleImages.images.Length);

                foreach (FacesTopLevelSingle faces in multipleImages.images)
                {
                    Log.Debug("WebCamRecognition", "\tsource_url: {0}, resolved_url: {1}", faces.source_url,
                        faces.resolved_url);
                    if (faces.faces != null)
                    {
                        foreach (OneFaceResult face in faces.faces)
                        {

                            if (face.face_location != null)
                            {
                                // Debug.Log("Face Location: " + face.face_location.left + ", " + ", " + ", ");
                                Log.Debug("WebCamRecognition", "\t\tFace location: {0}, {1}, {2}, {3}",
                                    face.face_location.left,
                                    face.face_location.top, face.face_location.width, face.face_location.height);

                                dataContainer.cropLeft = (int)face.face_location.left;
                                dataContainer.cropTop = (int)face.face_location.top;
                                dataContainer.cropWidth = (int)face.face_location.width;
                                dataContainer.cropHeight = (int)face.face_location.height;
                            }
                            if (face.gender != null)
                            {
                                Debug.Log("Gender: " + face.gender.gender);
                                Log.Debug("WebCamRecognition", "\t\tGender: {0}, Score: {1}", face.gender.gender,
                                    face.gender.score);

                                dataContainer.gender = face.gender.gender;
                                dataContainer.genderScore = (float)face.gender.score;
                            }
                            if (face.age != null)
                            {
                                Log.Debug("WebCamRecognition", "\t\tAge Min: {0}, Age Max: {1}, Score: {2}",
                                    face.age.min,
                                    face.age.max, face.age.score);

                                dataContainer.ageMin = face.age.min;
                                dataContainer.ageMax = face.age.max;
                                dataContainer.ageScore = (float)face.age.score;
                            }

                            if (face.identity != null)
                            {
                                Log.Debug("WebCamRecognition", "\t\tName: {0}, Score: {1}, Type Heiarchy: {2}",
                                    face.identity.name, face.identity.score, face.identity.type_hierarchy);

                                dataContainer.identity = face.identity.name;
                                dataContainer.identityScore = (float)face.identity.score;
                                dataContainer.typeHierarchy = face.identity.type_hierarchy;
                            }
                        }
                    }
                    else
                    {
                        Debug.Log("Faces.faces is null: " + faces.error.description);

                    }
                }
            }
        }
        else
        {
            Debug.Log("Detect faces failed");

            Log.Debug("WebCamRecognition", "Detect faces failed!");
        }

        detectingFace = false;
    }

    private void OnClassify(ClassifyTopLevelMultiple classify, string data)
    {
        Debug.Log("OnClassify");

        if (currentPersonIndex < 0 || currentPersonIndex >= people.Length)
        {
            Debug.Log("Index out of range!");
            detectingFace = false;
            return;
        }   

        var dataContainer = people[currentPersonIndex].emotionData;

        if (classify != null)
        {
            Debug.Log("Classify success!");

            Debug.Log("ExampleVisualRecognition images processed: " + classify.images_processed);
            foreach (ClassifyTopLevelSingle image in classify.images)
            {
                // Debug.Log("ExampleVisualRecognition source_url: " + image.source_url + ", resolved_url: " + image.resolved_url);
                Debug.Log("ExampleVisualRecognition image classifiers: " + image.classifiers.Length);

                if (image.classifiers != null && image.classifiers.Length > 0)
                {
                    foreach (ClassifyPerClassifier classifier in image.classifiers)
                    {
                        Debug.Log("classifier_id: " + classifier.classifier_id + ", name: " + classifier.name);
                        foreach (ClassResult classResult in classifier.classes)
                        {
                            Debug.Log("ExampleVisualRecognition class: " + classResult.m_class + ", score: " +
                                      classResult.score + ", type_hierarchy: " + classResult.type_hierarchy);

                            if (classResult.m_class == "happy")
                            {
                                dataContainer.happyScore = (float)classResult.score;
                            }
                            if (classResult.m_class == "angry")
                            {
                                dataContainer.angryScore = (float)classResult.score;
                            }
                            if (classResult.m_class == "eyewear")
                            {
                                dataContainer.glassesScore = (float)classResult.score;
                            }
                        }
                    }
                }
            }
        }
        else
        {
            Debug.Log("Classification failed!");
        }

        detectingFace = false;
    }
}
