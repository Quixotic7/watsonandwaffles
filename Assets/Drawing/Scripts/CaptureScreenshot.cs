﻿using System.IO;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CaptureScreenshot : MonoBehaviour
{
	private Camera cam;
	private static readonly KeyCode passKey = KeyCode.P;
	private static readonly KeyCode failKey = KeyCode.F;
	private static readonly KeyCode askKey = KeyCode.Space;
	private static readonly int resWidth = 1024;
	private static readonly int resHeight = 1024;

    private static readonly int inGameResWidth = 1024;
    private static readonly int inGameResHeight = 1024;

    void Awake()
	{
		cam = GetComponent<Camera>();
	}

	void Update ()
	{
		var pass = Input.GetKeyDown(passKey);
		var fail = Input.GetKeyDown(failKey); 
		if (pass || fail)
		{
			var bytes = Capture(cam);
			var dir = TargetDirectory(pass);
			Directory.CreateDirectory(dir);
			var filename = dir + ScreenShotName();
			File.WriteAllBytes(filename, bytes);
		}

	}

	public static string TargetDirectory(bool pass)
	{
		var subDir = pass ? "_positive_examples/" : "_negative_examples/";
		return System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/WatsonSVVRHack/" + subDir;
	}

	public static string ScreenShotName()
	{
		return Random.value + ".png";
	}

	public static byte[] Capture(Camera cam)
	{
		var rt = new RenderTexture(resWidth, resHeight, 24);
		cam.targetTexture = rt;
		var screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
		cam.Render();
		RenderTexture.active = rt;
		screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
		cam.targetTexture = null;
		RenderTexture.active = null; //http://answers.unity3d.com/questions/22954/how-to-save-a-picture-take-screenshot-from-a-camer.html
		Destroy(rt);
		return screenShot.EncodeToPNG();
	}


    public static byte[] CaptureForWatson(Camera cam)
    {
        var rt = new RenderTexture(resWidth, resHeight, 24);
        cam.targetTexture = rt;
        var screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
        cam.Render();
        RenderTexture.active = rt;
        screenShot.ReadPixels(new Rect(0, 0, inGameResWidth, inGameResHeight), 0, 0);
        cam.targetTexture = null;
        RenderTexture.active = null; //http://answers.unity3d.com/questions/22954/how-to-save-a-picture-take-screenshot-from-a-camer.html
        Destroy(rt);
        return screenShot.EncodeToJPG();
    }
}
