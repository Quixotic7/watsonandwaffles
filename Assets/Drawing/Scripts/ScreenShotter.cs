﻿using System.Collections;
using UnityEngine;
using System.IO;

public class ScreenShotter : MonoBehaviour {
	[SerializeField] private LayerMask screenshotCullingMask;
	[SerializeField] private LayerMask defaultCullingMask;
	[SerializeField] private CameraClearFlags screenshotClearFlags;
	[SerializeField] private CameraClearFlags defaultClearFlags;
	private Camera cam;
	private static readonly KeyCode askKey = KeyCode.Space;
	private Color defaultBackgroundColor;

    [SerializeField]
    private bool watsonMode;

    [SerializeField]
    private WatsonClassifyDrawing watsonDrawingClassifier;


    void Awake()
	{
		cam = GetComponent<Camera>();
		defaultBackgroundColor = cam.backgroundColor;
	}

    public void TakeScreenShot()
    {
        StartCoroutine(DoTakeScreenshot());
    }


    protected IEnumerator DoTakeScreenshot()
    {
        if (watsonMode)
        {
            Debug.Log("Classifying drawing");

            cam.cullingMask = screenshotCullingMask;
            cam.clearFlags = screenshotClearFlags;
            cam.backgroundColor = Color.black;

            var bytes = CaptureScreenshot.CaptureForWatson(cam);

            cam.cullingMask = defaultCullingMask;
            cam.clearFlags = defaultClearFlags;
            cam.backgroundColor = defaultBackgroundColor;

            yield return new WaitForEndOfFrame();

            var filename = CaptureScreenshot.ScreenShotName();

            File.WriteAllBytes(filename, bytes);

            yield return new WaitForEndOfFrame();

            watsonDrawingClassifier.ClassifyDrawing(bytes);

            yield return new WaitForEndOfFrame();
        }
        else
        {
            cam.cullingMask = screenshotCullingMask;
            cam.clearFlags = screenshotClearFlags;
            cam.backgroundColor = Color.black;

            var bytes = CaptureScreenshot.Capture(cam);

            cam.cullingMask = defaultCullingMask;
            cam.clearFlags = defaultClearFlags;
            cam.backgroundColor = defaultBackgroundColor;

            yield return new WaitForEndOfFrame();


            var filename = CaptureScreenshot.ScreenShotName();
            File.WriteAllBytes(filename, bytes);

            yield return new WaitForEndOfFrame();
        }

    }



	void Update () {
		var ask = Input.GetKeyDown(askKey);
		if (ask)
		{
		    TakeScreenShot();
			
		}
	}
}
