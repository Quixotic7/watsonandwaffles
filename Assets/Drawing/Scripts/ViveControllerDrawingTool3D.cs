﻿using UnityEngine;
using VRTK;
using System.Collections.Generic;

[RequireComponent(typeof(VRTK_ControllerEvents))]
public class ViveControllerDrawingTool3D : MonoBehaviour, DrawingTool3D
{
	private VRTK_ControllerEvents events;
	[SerializeField]
	private Layer layer;
	[SerializeField]
	private GameObject VisibleLinePrefab;

	private Line currLine;
	private List<Line> lines = new List<Line>();
	private GameObject debugDrawPoint;
	private bool isDrawing;
	void Awake()
	{
		var line = Instantiate(VisibleLinePrefab).GetComponent<Line>();
		if (line == null)
		{
			Debug.LogError("Did not provide a visible line.");
		}
		DestroyImmediate(line.gameObject);

		events = GetComponent<VRTK_ControllerEvents>();
        //events.TouchpadTouchStart += OnTouchpadTouchStart;
        //events.TouchpadTouchEnd += OnTouchpadTouchEnd;
        events.GripPressed += OnEraseDrawing;

	    events.TriggerPressed += OnDrawStart;
	    events.TriggerReleased += OnDrawEnd;

	}

	private void OnEraseDrawing(object sender, ControllerInteractionEventArgs e)
	{
		EraseIt();
	}



	public void EraseIt()
	{
		for (var i = 0; i < lines.Count; i++)
		{
			DestroyImmediate(lines[i].gameObject);
		}
		lines.Clear();
	}


    public void OnClassifyDone()
    {
        EraseIt();
    }

	private void OnDrawEnd(object sender, ControllerInteractionEventArgs e)
	{
		isDrawing = false;
		lines.Add(currLine);
	}

	void Update()
	{
		if (isDrawing)
		{
			currLine.AddPoint(GetDrawPoint());
		}
	}

	private void OnDrawStart(object sender, ControllerInteractionEventArgs e)
	{
		isDrawing = true;
		currLine = Instantiate(VisibleLinePrefab).GetComponent<Line>();
		currLine.gameObject.layer = (int)layer;
	}

	public Vector3 GetDrawPoint()
	{
		return transform.position;
	}
}
