﻿using UnityEngine;
using System.Collections;

public class changeDance : MonoBehaviour
{
    private Animator anim;

    private bool once;

    public Transform spot1;
    public Transform spot2;
    public Transform spot3;
    public Transform spot4;

    private bool finalSpot;

	void Start ()
    {
        anim = GetComponent<Animator>();

        once = true;

        finalSpot = true;
	}
	
	void Update ()
    {
        if (finalSpot)
        {
            if (this.name == "male1")
            {
                if (transform.position == spot1.position)
                {
                    DanceChange();
                    finalSpot = false;
                }
            }
            else if (this.name == "male2")
            {
                if (transform.position == spot2.position)
                {
                    DanceChange();
                    finalSpot = false;
                }
            }
            else if (this.name == "female1")
            {
                if (transform.position == spot3.position)
                {
                    DanceChange();
                    finalSpot = false;
                }
            }
            else if (this.name == "female2")
            {
                if (transform.position == spot4.position)
                {
                    DanceChange();
                    finalSpot = false;
                }
            }
        }
    }

    public void DanceChange()
    {
        int danceNum = Random.Range(1, 7);

        if (danceNum == 1)
        {
            anim.SetTrigger("anim1");
        }
        else if (danceNum == 2)
        {
            anim.SetTrigger("anim2");
        }
        else if (danceNum == 3)
        {
            anim.SetTrigger("anim3");
        }
        else if (danceNum == 4)
        {
            anim.SetTrigger("anim4");
        }
        else if (danceNum == 5)
        {
            anim.SetTrigger("anim5");
        }
        else if (danceNum == 6)
        {
            anim.SetTrigger("anim6");
        }
    }

    public void Run()
    {
        if(once)
        {
            if(this.name == "male1")
            {
                anim.SetTrigger("run");
                GetComponent<NavMeshAgent>().destination = spot1.position;
            }
            else if (this.name == "male2")
            {
                anim.SetTrigger("run");
                GetComponent<NavMeshAgent>().destination = spot2.position;
            }
            else if (this.name == "female1")
            {
                anim.SetTrigger("run");
                GetComponent<NavMeshAgent>().destination = spot3.position;
            }
            else if (this.name == "female2")
            {
                anim.SetTrigger("run");
                GetComponent<NavMeshAgent>().destination = spot4.position;
            }

            once = false;
        }
    }
}
