﻿using UnityEngine;
using System.Collections;

public class key : MonoBehaviour
{
    public GameObject exclude1;
    public GameObject exclude2;
    public GameObject exclude3;
    public GameObject exclude4;
    public GameObject exclude5;

    public GameObject doorR;
    public GameObject doorL;

	void Start ()
    {
	
	}
	
	void Update ()
    {
	    
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "key")
        {
            Destroy(other.gameObject);
            Destroy(exclude1.GetComponent<ExcludeTeleport>());
            Destroy(exclude2.GetComponent<ExcludeTeleport>());
            Destroy(exclude3.GetComponent<ExcludeTeleport>());
            Destroy(exclude4.GetComponent<ExcludeTeleport>());
            Destroy(exclude5.GetComponent<ExcludeTeleport>());
            doorR.transform.localEulerAngles += new Vector3(0f, 90f, 0f);
            doorL.transform.localEulerAngles += new Vector3(0f, -90f, 0f);
            Destroy(this.gameObject);
        }
    }
}