﻿using UnityEngine;
using System.Collections;

public class anmationGiver : MonoBehaviour
{
    private Animator anim;
    private CharacterFaceManager mood;
    private int ranStart = 0;

    private GameObject player;

	void Start ()
    {
        anim = GetComponent<Animator>();
        mood = GetComponent<CharacterFaceManager>();

        player = GameObject.Find("[CameraRig]");

        Invoke("DelayAnim", 1);
    }
	
	void Update ()
    {
	    if(GetComponent<CharacterFaceManager>().personData.texture2D.name == "happy10")
        {
            //Debug.Log(Vector3.Distance(player.transform.position, transform.position));
            if(Vector3.Distance(player.transform.position, transform.position) <= 20f)
            {
                //do the heart thing in order to interact with the character if within 20 units
            }
        }
	}

    void DelayAnim()
    {
        if (mood.personData.emotionData.angryScore > 0f && mood.personData.emotionData.happyScore > 0)
        {
            ranStart = Random.Range(0, 2);

            if (ranStart == 0)
            {
                anim.SetTrigger("neutral1");
            }
            else if (ranStart == 1)
            {
                anim.SetTrigger("neutral2");
            }
        }
        else if (mood.personData.emotionData.angryScore > 0f && mood.personData.emotionData.happyScore == 0f)
        {
            ranStart = Random.Range(0, 3);

            if (ranStart == 0)
            {
                anim.SetTrigger("mad1");
            }
            else if (ranStart == 1)
            {
                anim.SetTrigger("mad2");
            }
            else if (ranStart == 2)
            {
                anim.SetTrigger("mad3");
            }
        }
        else if (mood.personData.emotionData.angryScore == 0f && mood.personData.emotionData.happyScore > 0f)
        {
            ranStart = Random.Range(0, 3);

            if (ranStart == 0)
            {
                anim.SetTrigger("happy1");
            }
            else if (ranStart == 1)
            {
                anim.SetTrigger("happy2");
            }
            else if (ranStart == 2)
            {
                anim.SetTrigger("happy3");
            }
        }
        else if (mood.personData.emotionData.angryScore == 0f && mood.personData.emotionData.happyScore == 0f)
        {
            ranStart = Random.Range(0, 2);

            if (ranStart == 0)
            {
                anim.SetTrigger("neutral1");
            }
            else if (ranStart == 1)
            {
                anim.SetTrigger("neutral2");
            }
        }
    }

    public void DanceChange()
    {
        int danceNum = Random.Range(1, 7);

        if (danceNum == 1)
        {
            anim.SetTrigger("anim1");
        }
        else if (danceNum == 2)
        {
            anim.SetTrigger("anim2");
        }
        else if (danceNum == 3)
        {
            anim.SetTrigger("anim3");
        }
        else if (danceNum == 4)
        {
            anim.SetTrigger("anim4");
        }
        else if (danceNum == 5)
        {
            anim.SetTrigger("anim5");
        }
        else if (danceNum == 6)
        {
            anim.SetTrigger("anim6");
        }
    }
}
