﻿using UnityEngine;
using System.Collections;

public class spotlightHit : MonoBehaviour
{
    private bool once;
    private float timer;
    private bool stopTwerk;

	void Start ()
    {
        once = false;
        timer = 0f;
        stopTwerk = false;
	}
	
	void Update ()
    {
	   
	}

    RaycastHit rayHit;


    void FixedUpdate()
    {

        if (Physics.Raycast(transform.position, transform.forward, out rayHit, 7.5f))
        {
            if (rayHit.collider.tag == "model" && !once)
            {
                once = true;
            }
        }
        if(once && !stopTwerk && rayHit.collider != null)
        {
            timer += Time.deltaTime;
            if (timer >= 5f)
            {
                stopTwerk = true;
                rayHit.collider.gameObject.GetComponent<changeDance>().Run();
                timer = 0f;
            }
        }
    }
}