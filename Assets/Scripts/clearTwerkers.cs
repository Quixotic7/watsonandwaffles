﻿using UnityEngine;
using System.Collections;

public class clearTwerkers : MonoBehaviour
{
    private bool model;
    public GameObject teleportChange1;
    public GameObject teleportChange2;

	void Start ()
    {
        model = true;
	}
	
	void Update ()
    {
	    if(!model)
        {
            Destroy(teleportChange1.GetComponent<ExcludeTeleport>());
            Destroy(teleportChange2.GetComponent<ExcludeTeleport>());
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "model")
        {
            model = true;
        }
       
    }

    void OnTriggerExit(Collider other)
    {
        if(other.tag == "model")
        {
            model = false;
        }
    }
}