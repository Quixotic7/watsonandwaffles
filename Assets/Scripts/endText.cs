﻿using UnityEngine;
using System.Collections;

using UnityEngine;
using IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1;
using IBM.Watson.DeveloperCloud.Logging;

public class endText : MonoBehaviour {


     TextToSpeech m_TextToSpeech = new TextToSpeech();
    string m_TestString = "";

// Use this for initialization
void Start () {

        m_TextToSpeech.Voice = VoiceType.en_US_Allison;

        //Intro text
        m_TestString = "ALL YOUR WAFFLES ARE BELONG TO WATSON!";

        m_TextToSpeech.ToSpeech(m_TestString, HandleToSpeechCallback, true);
    }

    void HandleToSpeechCallback(AudioClip clip)
    {
        PlayClip(clip);
    }

    private void PlayClip(AudioClip clip)
    {
        if (Application.isPlaying && clip != null)
        {
            GameObject audioObject = new GameObject("AudioObject");
            AudioSource source = audioObject.AddComponent<AudioSource>();
            source.spatialBlend = 0.0f;
            source.loop = false;
            source.clip = clip;
            source.Play();

            GameObject.Destroy(audioObject, clip.length);
        }
    }
    // Update is called once per frame
    void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "[CameraRig]")
        {
            //put in text to speech here for the second text
            //Intro text
            m_TestString = "This looks like a good spot to place a ladder...or something";

            m_TextToSpeech.ToSpeech(m_TestString, HandleToSpeechCallback, true);
        }
    }
}
