﻿using UnityEngine;
using System.Collections;

public class ladder : MonoBehaviour
{
    public GameObject ladderObj;
    public GameObject unexclude1;
    public GameObject unexclude2;
    public GameObject ladderText;

	void Start ()
    {
	
	}
	
	void Update ()
    {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "ladder")
        {
            ladderObj.SetActive(true);
            Destroy(ladderText);
            Destroy(other.gameObject);
            Destroy(unexclude1.GetComponent<ExcludeTeleport>());
            Destroy(unexclude2.GetComponent<ExcludeTeleport>());
        }
    }
}