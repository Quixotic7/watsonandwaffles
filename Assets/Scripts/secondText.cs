﻿using UnityEngine;
using System.Collections;


using UnityEngine;
using IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1;
using IBM.Watson.DeveloperCloud.Logging;

public class secondText : MonoBehaviour {

    TextToSpeech m_TextToSpeech = new TextToSpeech();
    string m_TestString = "<speak version=\"1.0\"><say-as interpret-as=\"letters\">I'm sorry</say-as>. <prosody pitch=\"150Hz\">This is Text to Speech!</prosody></express-as><express-as type=\"GoodNews\">I'm sorry. This is Text to Speech!</express-as></speak>";
    
    // Use this for initialization
    void Start () {

        //put in text to speech here regarding the intro text
        LogSystem.InstallDefaultReactors();
        m_TextToSpeech.Voice = VoiceType.en_US_Allison;

        //Intro text
        m_TestString = "Someone has stolen all the waffles! Find the man with glasses, black hair, and a huge smile.He will help you on your journey to restore the  world of its precious golden, delicious waffles.";

        m_TextToSpeech.ToSpeech(m_TestString, HandleToSpeechCallback, true);
    }

    void HandleToSpeechCallback(AudioClip clip)
    {
        PlayClip(clip);
    }

    private void PlayClip(AudioClip clip)
    {
        if (Application.isPlaying && clip != null)
        {
            GameObject audioObject = new GameObject("AudioObject");
            AudioSource source = audioObject.AddComponent<AudioSource>();
            source.spatialBlend = 0.0f;
            source.loop = false;
            source.clip = clip;
            source.Play();

            GameObject.Destroy(audioObject, clip.length);
        }
    }


    // Update is called once per frame
    void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.name == "[CameraRig]")
        {
            //put in text to speech here for the second text
            //Intro text
            m_TestString = "This looks like a good spot to place a ladder...or something";

            m_TextToSpeech.ToSpeech(m_TestString, HandleToSpeechCallback, true);
        }
    }
}
